<?php
namespace App\Http\Repository;

use App\Http\Interface\LoginInterface;
use App\Http\Traits\RequestTrait;
use Illuminate\Support\Facades\Session;

class LoginRepository implements LoginInterface {

    use RequestTrait;

    public function login($email, $password)
    {
        $resp = $this->post(env('API_URL').'login', [''], ['email' => $email, 'password' => $password]);

        if ($resp['success'] == true){

        }
        dd();
    }
}

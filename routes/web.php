<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

Route::get('/politica-privacidade', function () {
    return view('politica_privacidade');
})->name('politica_privacidade');

Route::prefix('thenomad/')->group(function () {
    Route::prefix('login')->group(function () {
        Route::get('/', [\App\Http\Controllers\LoginController::class, 'index'])->name('login');

        Route::post('/', [\App\Http\Controllers\LoginController::class, 'login'])->name('auth.login');
    });
});

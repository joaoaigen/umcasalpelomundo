<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Http;

trait RequestTrait
{
    /*
     * We can send a request with Headers and query.
     * */
    public function get(string $url, array $headers = null, array $query = null): array
    {
        return Http::withHeaders($headers)->get($url, $query)->throw()->json();
    }

    public function post(string $url, array $headers = null, array $query = null): array
    {
        return Http::withHeaders($headers)->post($url, $query)->throw()->json();
    }

    public function getToken(string $url, string $token, array $query)
    {
        return Http::withToken($token)->get($url, $query)->throw()->json();
    }

    public function postToken(string $url, string $token, array $body)
    {
        return Http::withToken($token)->post($url, $body)->throw()->json();
    }
}

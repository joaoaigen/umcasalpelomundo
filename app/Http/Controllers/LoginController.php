<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Repository\LoginRepository;

class LoginController extends Controller
{

    public function index()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request, LoginRepository $conn)
    {
        $data = $request->validated();

        return $conn->login($data['email'], $data['password']);
    }
}

@extends('layouts.master-without-nav')

@section('title') Dicas de viagens, morar e trabalhar no exterior e muito mais! @endsection

@section('body')

    <body data-topbar="dark" data-layout="horizontal" class="container-fluid">
    @endsection

    @section('content')

        {{--@component('components.breadcrumb')
            @slot('li_1') Links @endslot
            @slot('title') Links úteis @endslot
        @endcomponent--}}
        <div class="row d-flex align-content-center justify-content-center">
            <div class="col-md-12">
                <div class="d-flex align-content-center justify-content-center mb-3">
                    <img src="{{ URL::asset('/assets/images/users/avatar-1.jpg') }}" alt=""
                         class="rounded-circle" width="115px" height="115px"><br><br>
                </div>
            </div>

            <div class="col-md-12 d-flex align-content-around justify-content-center mb-3">
                <a type="button" class="btn btn-default" target="_blank"
                   href="https://www.instagram.com/umcasal.pelomundo/">
                    <i class="bx bxl-instagram align-middle me-2" style="font-size: 35px"></i>
                </a>
                <a type="button" class="btn btn-default" target="_blank"
                   href="https://www.facebook.com/umcasal.pelomundo/">
                    <i class="bx bxl-facebook align-middle me-2" style="font-size: 35px"></i>
                </a>
                <a type="button" class="btn btn-default" target="_blank"
                   href="https://www.youtube.com/channel/UCzJTonBrULsZyRVfJRDVRsw">
                    <i class="bx bxl-youtube align-middle me-2" style="font-size: 35px"></i>
                </a><br>
            </div>
        </div>

        <div class="row d-flex align-content-center justify-content-center">

            <div class="col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-img-top">
                    </div>
                    <div class="card-header">
                        <div class="d-flex align-content-center justify-content-center">
                            <h1 class="card-title font-size-24">👩🏼‍🤝‍👨🏽 Um Casal Pelo Mundo 🌍</h1>
                        </div>
                    </div>
                    <div class="card-body d-flex align-content-center justify-content-center"
                         style="text-align: center">
                        <p class="font-size-16">
                            Esperamos poder agregar valor a vocês com esses links! Seria muito importante para nós se
                            você nos desse seu Feedback,
                            conte-nos também o que mais querem ver por aqui! 😁
                        </p>
                    </div>
                </div>
            </div>

        </div>

        <div class="row d-flex align-content-center justify-content-center">
            <div class="col-md-6 col-lg-6">
                <div class="card">

                    {{--TITULO DO CARD--}}
                    <div class="card-header text-center">
                        <h3 class="card-title font-size-18">Modelo de currículo em Inglês <span
                                class="text-danger">PRONTO</span>!</h3>
                    </div>

                    {{--BODY DO CARD COM TEXTO EXPLICATIVO--}}
                    <div class="card-body">

                        {{--TEXTO EXPLICATIVO DO CARD--}}
                        <div class="row">

                            {{--TEXTO EXPLICATIVO DO CARD--}}
                            <div class="col-md-12 col-12" style="text-align: center">
                                <p class="font-size-15">
                                    Temos um video explicativo no nosso Insta de como preencher o currículo
                                    corretamente!
                                </p>
                            </div>

                            {{--BOTAO DE DIRECIONAMENTO PARA O SITE--}}
                            <div class="col-md-12">
                                <div class="d-flex align-content-center justify-content-center">
                                    <a type="button" target="_blank"
                                       class="btn btn-outline-warning waves-effect waves-light" style="font-size: 20px;"
                                       href="https://docs.google.com/document/d/1BII7FvXWu3T-cRO3SyDJbNPBoEVg7KRB/edit?usp=sharing&ouid=104082945224974152208&rtpof=true&sd=true">
                                        <div class="pt-2 mb-2">
                                            <i class="bx bx-file font-size-24 align-middle me-2"></i>
                                            BAIXAR AGORA
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-6">
                <div class="card">
                    {{--TITULO DO CARD--}}
                    <div class="card-header text-center">
                        <h3 class="card-title font-size-18">Empresas da Inglaterra que oferecem <span
                                class="text-danger">VISTO</span>
                            de <span class="text-danger">TRABALHO</span></h3>
                    </div>

                    {{--BODY DO CARD COM TEXTO EXPLICATIVO--}}
                    <div class="card-body">
                        <div class="row">
                            {{--TEXTO EXPLICATIVO DO CARD--}}
                            <div class="col-md-12 col-12" style="text-align: center">
                                <p class="font-size-15">
                                    Você pode pesquisar quais Empresas aqui da Inglaterra na sua área que estão
                                    oferecendo
                                    visto de trabalho, e pode aplicar para essas vagas!
                                </p>
                            </div>

                            {{--BOTAO DE DIRECIONAMENTO PARA O SITE--}}
                            <div class="col-md-12">
                                <div class="d-flex align-content-center justify-content-center">
                                    <a type="button" target="_blank"
                                       class="btn btn-outline-warning waves-effect waves-light"
                                       href="https://ukhired.com/" style="font-size: 18px">
                                        <div class="pt-2 mb-2 ">
                                            <i class="bx bx-chevron-right font-size-24 align-middle me-2"></i>
                                            Ir para o site
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-6">
                <div class="card">
                    {{--TITULO DO CARD--}}
                    <div class="card-header text-center">
                        <h3 class="card-title font-size-18">Total Jobs -> Encontre vagas de trabalho</h3>
                    </div>

                    {{--BODY DO CARD COM TEXTO EXPLICATIVO--}}
                    <div class="card-body">
                        <div class="row">

                            {{--TEXTO EXPLICATIVO DO CARD--}}
                            <div class="col-md-12 col-12" style="text-align: center">
                                <p class="font-size-15">
                                    Neste site você pode pesquisar vagas de trabalho da sua área e também pode pesquisar
                                    vagas que oferecem visto de trabalho,
                                    basta apenas pesquisar por <span class="text-danger">"TIER 2"</span> ou <span
                                        class="text-danger">"Sponsorship"</span>!
                                </p>
                            </div>

                            {{--BOTAO DE DIRECIONAMENTO PARA O SITE--}}
                            <div class="col-md-12">
                                <div class="d-flex align-content-center justify-content-center">
                                    <a type="button" target="_blank"
                                       class="btn btn-outline-warning waves-effect waves-light"
                                       href="https://www.totaljobs.com/" style="font-size: 18px">
                                        <div class="pt-2 mb-2">
                                            <i class="bx bx-chevron-right font-size-24 align-middle me-2"></i> Ir para o
                                            site
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-6">
                <div class="card">
                    {{--TITULO DO CARD--}}
                    <div class="card-header text-center">
                        <h3 class="card-title font-size-18">Indeed -> Encontre vagas de trabalho</h3>
                    </div>

                    {{--BODY DO CARD COM TEXTO EXPLICATIVO--}}
                    <div class="card-body">
                        <div class="row">

                            {{--TEXTO EXPLICATIVO DO CARD--}}
                            <div class="col-md-12 col-12" style="text-align: center">
                                <p class="font-size-15">
                                    Neste site você pode pesquisar vagas de trabalho da sua área e também pode pesquisar
                                    vagas que oferecem visto de trabalho,
                                    basta apenas pesquisar por <span class="text-danger">"TIER 2"</span> ou <span
                                        class="text-danger">"Sponsorship"</span>!
                                </p>
                            </div>

                            {{--BOTAO DE DIRECIONAMENTO PARA O SITE--}}
                            <div class="col-md-12">
                                <div class="d-flex align-content-center justify-content-center">
                                    <a type="button" target="_blank"
                                       class="btn btn-outline-warning waves-effect waves-light"
                                       href="https://uk.indeed.com/" style="font-size: 18px">
                                        <div class="pt-2 mb-2">
                                            <i class="bx bx-chevron-right font-size-24 align-middle me-2"></i>
                                            Ir para o site
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-6">
                <div class="card">
                    {{--TITULO DO CARD--}}
                    <div class="card-header text-center">
                        <h3 class="card-title font-size-18">Deepl -> Um dos melhores tradutores</h3>
                    </div>

                    {{--BODY DO CARD COM TEXTO EXPLICATIVO--}}
                    <div class="card-body">
                        <div class="row">

                            {{--TEXTO EXPLICATIVO DO CARD--}}
                            <div class="col-md-12 col-12" style="text-align: center">
                                <p class="font-size-15">
                                    Quando se esta começando a aprender o Inglês, ou quando surge uma dúvida de tradução
                                    e
                                    sempre bom consultar um tradutor. O Deepl ele e muito melhor que o
                                    Google Translate, pelo fato da tradução não ficar com aspecto robótico! O Deepl usa
                                    uma
                                    Inteligência Artificial para fazer as traduções!
                                </p>
                            </div>

                            {{--BOTAO DE DIRECIONAMENTO PARA O SITE--}}
                            <div class="col-md-12">
                                <div class="d-flex align-content-center justify-content-center">
                                    <a type="button" target="_blank"
                                       class="btn btn-outline-warning waves-effect waves-light"
                                       href="https://www.deepl.com/translator" style="font-size: 18px">
                                        <div class="pt-2 mb-2">
                                            <i class="bx bx-chevron-right font-size-24 align-middle me-2"></i>
                                            Ir para o site
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
@endsection

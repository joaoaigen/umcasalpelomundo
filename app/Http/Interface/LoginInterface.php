<?php

namespace App\Http\Interface;

interface LoginInterface{
    public function login($email, $password);
}

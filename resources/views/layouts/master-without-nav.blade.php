<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8"/>
    <title> @yield('title') | Dicas, Viagens, morar no exterior</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5610101891038206"
            crossorigin="anonymous"></script>
    <meta name="description"
          content="Gostamos de viajar o mundo e queremos levar você conosco nessa jornada! Damos dicas de como morar e trabalhar no exterior, como viajar barato e muito mais!">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('/assets/images/users/avatar-1.jpg') }}">

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '623726078941407');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=623726078941407&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-219838631-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-219838631-1');
    </script>

    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5610101891038206"
            crossorigin="anonymous"></script>
    @include('layouts.head-css')

    <style>
        @media only screen and (max-width: 600px) {
            html, body {
                overflow-x: hidden;
            }
        }

        @media only screen and (max-width: 60000px) {
            html, body {
                overflow-x: hidden;
            }
        }
    </style>
</head>
<div id="layout-wrapper">
@yield('body')

    {{--@include('layouts.topbar')--}}
    <div class="page-content">
        @yield('content')
    </div>

    @include('layouts.footer')
</div>

@include('layouts.vendor-scripts')

</body>
</html>
